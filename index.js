const express = require('express')
const app = express()
const PORT = 8080;

const fs = require('fs')
let users = require('./users.json')

app.use(express.json());

const {UserGame, UserGameBiodata} = require('./models');

app.use(
  express.urlencoded({
      extended: false
  })
)

//view engine
app.set('view engine', 'ejs')

//CREATE users
app.get('/signup', (req, res) => {
  res.render('create-user')
})

app.post('/signup', (req, res) =>{
  const {username, password} = req.body

  UserGame.create({
    username, 
    password,
    userGameBiodata: {
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      birthday: req.body.birthday,
      gender: req.body.gender,
      phone: req.body.phone,
      location: req.body.location
    }
  }, {
    include: [{
      model: UserGameBiodata,
      as: "UserGameBiodatum"
    }]
  }).then(user =>{
    res.redirect('/users')
  }).catch(err => {
    console.log(err)
    res.sendStatus(500)
  })
})

//READ
app.get('/users', (req, res) => {
  UserGame.findAll()
      .then(user =>{
        res.render('allUsers', {
          user
        })
      })
});

//get the users
app.get('/login', (req, res) => {
  const {username, password, memo} = req.body

  if (!username || !password){
    res.status(400).json({
      memo: 'please fill out'
    })
  }

  const existingUser = users.find(user => user.username === username)

  if (!existingUser || existingUser.password !== password){
    res.sendStatus(401)
    return
  }

  res.status(200).json({
    name: existingUser.name,
    username: existingUser.username
  })
})

//update

//display update
app.get('/users/update/:id', (req, res) =>{
  res.render('update')
})

app.post('/users/update/:id', (req, res) =>{
  const {username, password} = req.body
  UserGame.update({
    password
  }, {
    where: {id: req.params.id}
  }).then(user =>{
    res.render('users')
  })
})

//Delete
app.get('/users/delete/:id', (req, res) =>{
  UserGame.destroy({
    where: {id: req.params.id}
  }).then(() => res.redirect('/users'))
})

//port
app.listen(PORT, () => {
    console.log(`Server is listening on ${PORT}`);
})