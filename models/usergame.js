'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class UserGame extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      //disebutkan if not, it looks for UserGameId
      this.hasMany(models.UserGameHistory, {foreignKey: 'user_id', as: "UserGameBiodatum"})
      this.hasOne(models.UserGameBiodata, {foreignKey: 'user_id'})
    }
  }
  UserGame.init({
    username: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'UserGame',
  });
  return UserGame;
};